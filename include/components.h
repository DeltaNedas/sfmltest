#pragma once

#include "uuid.h"

#include <Box2D/Dynamics/b2Body.h>
#include <SFML/Graphics.hpp>

#include <set>

class Entity;
class EntityTransform;
class Component {
public:
	Component();
	virtual ~Component();

	virtual void added();

	UUID getUUID();

	Entity* entity = nullptr;
protected:
	UUID uuid;
};

class CTransform : public Component {
public:
	CTransform(EntityTransform* transform = nullptr);
	~CTransform();

	void added();

	EntityTransform* transform;
};

class CHealth : public Component {
public:
	CHealth(float maxHealth = 0);
	~CHealth();

	float maxHealth, health;
};

/* Rendering */

class CDrawable : public CTransform {
public:
	CDrawable(sf::Drawable* drawable = nullptr);
	~CDrawable();

	sf::Drawable* drawable; // Specific fields like "sprite" are just this pointer but casted.
};

class CSprite : public CDrawable {
public:
	CSprite(std::string file);
	CSprite(sf::Sprite* sprite);
	~CSprite();

	sf::Sprite* sprite;
};
class CShape : public CDrawable {
public:
	CShape(size_t sides, float length);
	CShape(sf::Shape* shape);
	~CShape();

	sf::Shape* shape;
};
class CText : public CDrawable {
public:
	CText(std::string text, std::string font = "default.ttf", size_t size = 12, sf::Color color = sf::Color::White);
	~CText();

	sf::Font* font;
	sf::Text* text;
};

// Not exactly ECS now, is it...

class CUpdate : public Component {
public:
	CUpdate(std::function<void(void)> func);
	~CUpdate();

	std::function<void(void)> func;
};

/* Physics */

class CPhysics : public CTransform {
public:
	CPhysics(float mass = 1, float friction = 0);
	~CPhysics();

	float mass, // mass in kg
		friction; // 0: velocity is untouched -> 1: velocity is entirely removed
	b2Body* body = nullptr;
};
/*class CStatic : public CPhysics {
};
class CDynamic : public CPhysics {
};*/

extern std::set<Component*> components;
extern std::set<CTransform*> transformComponents;
extern std::set<CHealth*> healthComponents;

extern std::set<CDrawable*> drawableComponents; // Accessed in render thread
extern std::set<CSprite*> spriteComponents;
extern std::set<CShape*> shapeComponents;
extern std::set<CText*> textComponents;

extern std::set<CUpdate*> updateComponents;
extern std::set<CPhysics*> physicsComponents;