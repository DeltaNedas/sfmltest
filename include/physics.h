#pragma once

#include <Box2D/Common/b2Math.h>
#include <Box2D/Dynamics/b2World.h>

#define VELOCITY_ITERATIONS 6
#define POSITION_ITERATIONS 2

extern b2Vec2 gravity;
extern b2World* world;