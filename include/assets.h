#pragma once

#include "utils.h"

#include <SFML/Graphics.hpp>

#include <filesystem>
#include <map>

extern std::string root;

using recursive_directory_iterator = std::filesystem::recursive_directory_iterator;
typedef std::function<void(std::string, std::string)> Loader;

template <typename T>
class Assets {
public:
	Assets() {}
	Assets(std::string path, std::string fallback, Loader loader) {
		this->path = root + path;
		this->fallback = fallback;
		this->loader = loader;
	}

	// Print error and return exit code, if an error occurs.
	void load() {
		try {
			for (const auto& dirEntry : recursive_directory_iterator(path)) {
				std::string file(dirEntry.path().string());
				loader(file, filename(file));
				fprintf(stderr, "[DEBUG]\tLoaded asset \"%s\".\n", file.c_str());
			}
		} catch (std::exception& e) {
			fprintf(stderr, "[FATAL]\tFailed to load assets at %s: %s\n", path.c_str(), e.what());
			throw ENOENT;
		}
	}

	void free() {
		puts("RIP assets");
		for (auto& it : values) {
			printf("Delet %s\n", it.first.c_str());
			//delete it.second;
		}
		puts("No???");
	}

	T* get(std::string key) {
		auto it = values.find(key);
		if (it == values.end()) {
			if (key == fallback) {
				return NULL;
			}
			T* fallback = get(this->fallback);
			if (!fallback) {
				fprintf(stderr, "[ERROR]\tFailed to find fallback asset for %s/%s!\n", path.c_str(), key.c_str());
			}
			return fallback;
		}
		return it->second;
	}

	std::map<std::string, T*> values;

protected:
	std::string path;
	std::string fallback = "default";
	Loader loader;
};

void loadAssets();

extern Assets<sf::Font> fonts;
extern Assets<sf::Texture> textures;