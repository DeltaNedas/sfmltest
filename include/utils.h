#pragma once
#include "types.h"

#include <string>
#include <vector>

#include <uuid/uuid.h>

/* Time */

nanos_t timestamp();

/* File sutff */

std::string filename(std::string path); // Basename

/* General string utils */

bool compare(std::string str, std::vector<std::string> comp);
std::string concat(std::vector<std::string> strings, std::string by = "");
std::string lower(std::string str);

/* Generics */

// Simple way to get component types
template <typename Base, typename T>
const Base* extends(T* obj) {
	return dynamic_cast<const Base*>(obj);
}