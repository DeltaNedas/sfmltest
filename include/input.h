#pragma once

#include <functional>
#include <map>
#include <vector>

#include <SFML/Window/Event.hpp>

typedef std::function<void(sf::Event)> EventListener;
typedef std::function<void(sf::Event::KeyEvent, bool /* pressed */)> KeyListener;

void callListeners(sf::Event);
size_t addListener(sf::Event::EventType type, EventListener listener);
size_t addListener(sf::Keyboard::Key key, KeyListener listener);
void removeListener(sf::Event::EventType type, size_t index);
void removeListener(sf::Keyboard::Key, size_t index);

extern std::map<sf::Event::EventType, std::vector<EventListener>> listeners;
extern std::map<sf::Keyboard::Key, std::vector<KeyListener>> keyListeners;