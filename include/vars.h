#pragma once

#include "types.h"

#include <SFML/Graphics.hpp>

extern sf::RenderWindow* window;

extern bool debug; // Disable signal handling and show message
extern size_t frameLimit; // in Hz
extern nanos_t frameRate;
extern const nanos_t tickRate; // Tickrate for update, physics.
extern const nanos_t inputRate; // Fast tickrate for input loop only.
extern const float pixelScale; // 1 unit = X pixels to SFML