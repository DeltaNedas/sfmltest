#pragma once

#include <uuid/uuid.h>

#include <string>

// Wrapper over libuuid
class UUID {
public:
	UUID();
	UUID(uuid_t data);
	UUID(std::string parse); // Parse a hex string in the format 8-4-4-4-12
	operator std::string() const;

	uuid_t data;
	bool valid = false;
};