#pragma once

typedef unsigned long nanos_t;
const nanos_t toSeconds = 1e9;
const nanos_t toMicros = 1e6;
const nanos_t toMillis = 1e3;

typedef unsigned short health_t;