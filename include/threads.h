#pragma once

#include "types.h"
#include "vars.h"

#include <map>
#include <thread>

int start();
void quit(int code = 0);

class Ticker {
public:
	Ticker() {}
	Ticker(nanos_t rate, nanos_t speed = 100);

	void sleep();

	nanos_t rate = 0, speed = 0, last = 0, delta = 0;
};

class Threaded {
public:
	Threaded(std::string name, int(*func)(Threaded*), Ticker tick, bool detach = true);

	std::string name;
	std::thread* thread = nullptr;
	Ticker tick;
	bool running = false;
};

int renderLoop(Threaded* thread); // First thread
int updateLoop(Threaded* thread);
int physicsLoop(Threaded* thread);
int inputLoop(Threaded* thread); // Main thread, joined

extern bool running;
extern std::map<std::string, Threaded*> threads;