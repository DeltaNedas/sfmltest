#pragma once

#include "uuid.h"
#include "utils.h"
#include "vars.h"

#include <set>

class EntityTransform : public sf::Transformable {
public:
	EntityTransform(float x = 0, float y = 0, float w = 1, float h = 1, float rot = 0);

	void setPosition(float x, float y);
	void setPosition(const sf::Vector2f& pos);
	void setRotation(float rot);
	void setScale(float w, float h);
	void setScale(const sf::Vector2f& scale);
	sf::Transform getTransform();

	float x, y, rot, w, h;
};

class Component;
class Entity {
public:
	Entity(std::string name, EntityTransform* transform, std::set<Component*> components = {});
	virtual ~Entity();

	void add(Component* add);
	void add(std::set<Component*> add);

	// Return first component of a type
	template <class C>
	const C* getComponent(C* helper = nullptr) {
		for (Component* c : components) {
			const C* ptr = extends<C>(c);
			if (ptr) {
				return ptr;
			}
		}
		return nullptr;
	}
	inline sf::Transform getTransform() {
		if (transform) {
			return transform->getTransform();
		}
		return sf::Transform::Identity;
	}
	UUID getUUID();

	std::string name;
	EntityTransform* transform;
	std::set<Component*> components;
protected:
	UUID uuid;
};

class EWood : public Entity {
public:
	EWood(std::string name, EntityTransform* transform, std::set<Component*> extraComponents = {});
};

class EAtom : public Entity {
public:
	EAtom(std::string name, EntityTransform* transform, int protons, int neutrons, int charge = 0, std::set<Component*> extraComponents = {});

	void addElectron(int total, int shell, int e, float nucleus);
};

extern std::set<Entity*> entities;

void loadEntities();