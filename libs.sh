#!/bin/sh
try() {
	which $1 > /dev/null && use_$2 && exit
}

use_apt() {
	apt-get install \
		libsfml-dev \
		libbox2d-dev \
		uuid-dev
}

# may not work, havent tested
use_dnf() {
	dnf install \
		compat-SFML16-devel  \
		box2d-devel \
		uuid-devel
}

try apt-get apt
try dnf dnf

echo "Please install SFML, Box2D and libuuid headers"
exit 1
