#include "utils.h"

#include <libgen.h>
#include <string.h>
#include <time.h>

nanos_t timestamp() { // In nanoseconds
	struct timespec spec;
	clock_gettime(CLOCK_REALTIME, &spec);

	return spec.tv_sec * toSeconds + spec.tv_nsec;
}

std::string filename(std::string path) {
	char* buffer = strdup(path.c_str());
	if (buffer == NULL) {
		fprintf(stderr, "[FATAL]\tOut of memory!\n");
		exit(ENOMEM);
	}

	std::string ret(basename(buffer));
	free(buffer);
	return ret;
}

bool compare(std::string str, std::vector<std::string> comp) {
	for (std::string s : comp) {
		if (s == str) {
			return true;
		}
	}
	return false;
}

std::string concat(std::vector<std::string> strings, std::string by) {
	std::string result = "";

	if (strings.size() > 0) {
		for (std::string string : strings) {
			result += string + by;
		}
		result.erase(result.end() - by.size(), result.end());
	}
	return result;
}

std::string lower(std::string string) {
	std::string output;
	output.reserve(string.size());
	for (const char c : string) {
		if (c >= 'A' && c <= 'Z') {
			output.push_back(c + 32);
		} else {
			output.push_back(c);
		}
	}

	return output;
}