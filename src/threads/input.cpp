#include "threads.h"

#include "input.h"
#include "utils.h"

#ifdef __linux__
#	include <X11/Xlib.h>
#endif

using namespace sf;

int inputLoop(Threaded* thread) {
	Ticker* tick = &thread->tick;
	Event event;
	thread->running = true;

	// Set up input listeners
	addListener(Event::Closed, std::function([=](Event e){ quit(); }));

	try {
		while (running) {
			tick->sleep();

			#ifdef __linux__
			//	XLockDisplay((Display*) window->getSystemHandle());
			#endif
			while (window->pollEvent(event)) {
				callListeners(event);
			}
			#ifdef __linux__
			//	XUnlockDisplay((Display*) window->getSystemHandle());
			#endif
		}
	} catch (std::exception& e) {
		printf("[FATAL]\tInput thread died: %s\n", e.what());
		quit(1);
	}

	thread->running = false;
	return 0;
}