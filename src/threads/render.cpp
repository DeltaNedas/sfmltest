#include "threads.h"

#include "components.h"
#include "entities.h"

#include <SFML/Graphics.hpp>

#include <stdio.h>

using namespace sf;

// Only access window here or X will cry
int renderLoop(Threaded* thread) {
	Ticker* tick = &thread->tick;
	thread->running = true;
	try {
		while (window->isOpen() && running) {
			tick->sleep();

			window->clear();
			for (CDrawable* c : drawableComponents) {
				window->draw(*c->drawable, c->transform ? c->transform->getTransform() : Transform::Identity);
			}
			window->display();
		}
	} catch (std::exception& e) {
		printf("[FATAL]\tRender thread died: %s\n", e.what());
		quit(1);
	}

	thread->running = false;
	return 0;
}