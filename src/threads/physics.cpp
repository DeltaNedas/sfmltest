#include "threads.h"

#include "entities.h"
#include "components.h"
#include "physics.h"
#include "utils.h"

#include <Box2D/Collision/Shapes/b2PolygonShape.h>
#include <Box2D/Dynamics/b2Fixture.h>

using namespace sf;

int physicsLoop(Threaded* thread) {
	Ticker* tick = &thread->tick;
	thread->running = true;
	try {
		// Initialize world and initial positions
		world = new b2World(gravity);
		for (CPhysics* c : physicsComponents) {
			EntityTransform* t = c->transform;
			b2BodyDef body;
			body.type = b2_dynamicBody;
			body.position.Set(t->x, t->y);
			body.angle = t->rot;
			c->body = world->CreateBody(&body);

			b2PolygonShape hitbox;
			hitbox.SetAsBox(t->x / 2, t->y / 2);

			b2FixtureDef fixture;
			fixture.shape = &hitbox;
			fixture.density = c->mass / t->x * t-> y;
			fixture.friction = c->friction;
			c->body->CreateFixture(&fixture);
		}

		while (running) {
			tick->sleep();
			world->Step(1 / 60, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
			for (CPhysics* c : physicsComponents) {
				EntityTransform* t = c->transform;
				b2Vec2 position = c->body->GetPosition();
				t->rot = c->body->GetAngle();
				t->setPosition(position.x, position.y);
				//printf("Physics %s now at %g,%g\n", std::string(c->getUUID()).c_str(), t->x, t->y);
			}
			// FIXME: stuff isnt moved
			// TODO: physics stuff
		}
	} catch (std::exception& e) {
		printf("[FATAL]\tPhysics thread died: %s\n", e.what());
		quit(1);
	}

	thread->running = false;
	return 0;
}