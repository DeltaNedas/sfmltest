#include "threads.h"

#include "components.h"

int updateLoop(Threaded* thread) {
	Ticker* tick = &thread->tick;
	thread->running = true;
	try {
		while (running) {
			tick->sleep();
			for (CUpdate* c : updateComponents) {
				c->func(); // Not very ecs-y but oh well
			}
		}
	} catch (std::exception& e) {
		printf("[FATAL]\tUpdate thread died: %s\n", e.what());
		quit(1);
	}

	thread->running = false;
	return 0;
}