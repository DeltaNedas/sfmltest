#include "threads.h"

#include "assets.h"
#include "utils.h"

#ifdef __linux__
#	include <X11/Xlib.h>
#endif

using namespace sf;

int exitCode = 0;

int start() {
	frameRate = toSeconds / frameLimit;
	printf("Frame limit is %lu/%lu\n", frameRate, inputRate);

	#ifdef __linux__
	XInitThreads();
	#endif
	window = new RenderWindow(VideoMode(320, 180), "SFML Test");
	window->setVerticalSyncEnabled(true);
	window->setFramerateLimit(frameRate);
	Texture* icon = textures.get("icon.png");
	if (icon) {
		Vector2 size = icon->getSize();
		window->setIcon(size.x, size.y, icon->copyToImage().getPixelsPtr());
	}
	running = true;

	new Threaded("render", renderLoop, Ticker(frameRate));
	new Threaded("update", updateLoop, Ticker(tickRate));
	new Threaded("physics", physicsLoop, Ticker(tickRate));
	//new Threaded("render", renderLoop, Ticker(frameRate), false);
	new Threaded("input", inputLoop, Ticker(inputRate), false);

	// Wait for all threads to safely return.
	for (auto& it : threads) {
		Threaded* thread = it.second;
		while (thread->running) {
			sf::sleep(sf::milliseconds(1));
		}
	}

	window->close();
	return exitCode;
}

void quit(int code) {
	running = false;
	exitCode = code;
}

Ticker::Ticker(nanos_t rate, nanos_t speed) {
	this->rate = rate;
	this->speed = speed;
	last = timestamp();
}

void Ticker::sleep() {
	while ((timestamp() - last) < rate) {
		sf::sleep(sf::microseconds(speed));
	}
	delta = timestamp() - last;
	last += delta;
}

Threaded::Threaded(std::string name, int(*func)(Threaded*), Ticker tick, bool detach) {
	this->tick = tick;
	this->name = name;
	thread = new std::thread(func, this);
	threads[name] = this;
	if (detach) {
		thread->detach();
	} else {
		thread->join();
	}
}

bool running = false;
std::map<std::string, Threaded*> threads = {};