#include "entities.h"

#include "angles.h"
#include "components.h"
#include "threads.h"
#include "vars.h"

#include <math.h>

#define min(a, b) a < b ? a : b

using namespace sf;

EntityTransform::EntityTransform(float x, float y, float w, float h, float rot) {
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->rot = fmod(1 + rot, 1);
}
void EntityTransform::setPosition(float x, float y) {
	this->x = x;
	this->y = y;
}
void EntityTransform::setPosition(const sf::Vector2f& pos) {
	this->x = pos.x;
	this->y = pos.y;
}
void EntityTransform::setRotation(float rot) {
	this->rot = rot;
}
void EntityTransform::setScale(float w, float h) {
	this->w = x;
	this->h = y;
}
void EntityTransform::setScale(const sf::Vector2f& scale) {
	this->w = scale.x;
	this->h = scale.y;
}
Transform EntityTransform::getTransform() {
	Transform ret;
	//auto center = window->getSize();
	//ret.translate(center.x / 720 / 2, center.y / 1280 / 2);
	ret.translate(x * pixelScale, y * pixelScale);
	ret.translate(w * pixelScale / -2, h * pixelScale / -2);
	ret.scale(w, h);
	ret.rotate(rot * 360);
	return ret;
}

Entity::Entity(std::string name, EntityTransform* transform, std::set<Component*> components) {
	this->name = name;
	this->uuid = UUID();
	this->transform = transform;
	if (debug) {
		printf("[DEBUG]\tNew entity %s with id %s - %p\n", name.c_str(), std::string(uuid).c_str(), (void*) transform);
	}
	add(components);
	entities.insert(this);
}
Entity::~Entity() {
	for (Component* c : components) {
		delete c;
	}
	delete transform;
	entities.erase(this);
}

void Entity::add(Component* add) {
	components.insert(add);
	add->entity = this;
	add->added();
}
void Entity::add(std::set<Component*> add) {
	if (add.size()) {
		components.insert(add.begin(), add.end());
		for (Component* c : add) {
			c->entity = this;
			c->added();
		}
	}
}

UUID Entity::getUUID() {
	return uuid;
}

std::set<Entity*> entities = {};


EWood::EWood(std::string name, EntityTransform* transform, std::set<Component*> extraComponents) : Entity(name, transform) {
	float area = transform->w * transform->h;
	add({
		new CPhysics(3 * area, 0.1),
		new CHealth(25 * area),
		new CSprite(name + ".png")
	});
	add(extraComponents);
}

EAtom::EAtom(std::string name, EntityTransform* transform, int protons, int neutrons, int charge, std::set<Component*> extraComponents) : Entity(name, transform) {
	int electrons = protons - charge;
	float nucleus = protons + neutrons, nucleusSize = 4 * log(nucleus);
	CShape* nucleusS = new CShape(2 * nucleus, nucleusSize);
	nucleusS->shape->setFillColor(Color::Red);
	add(nucleusS); // Sides is consant, size is log n

	if (electrons > 0) {
		nucleus += 0.5; // Electron distance from nucleus
		nucleus /= pixelScale;
		int innerShell = min(electrons, 2);
		for (int e = 1; e <= innerShell; e++) {
			addElectron(innerShell, 1, e, nucleus);
		}

		if (electrons > 2) {
			int shells = ceil(electrons / 8);
			electrons -= 2;
			for (int shell = 1; shell <= shells; shell++) {
				int total = (electrons - 8 * (shell - 1)) % 8, remaining = total;
				for (int e = 1; e <= remaining; e++) {
					addElectron(total, shell + 1, e, nucleus);
				}
			}
		}
	}
	add(extraComponents);
}

void EAtom::addElectron(int total, int shell, int e, float nucleus) {
	double rot = (e - 1.0) / total + transform->rot;
	CShape* electron = new CShape(12, shell);
	electron->shape->setFillColor(Color::Blue);

	electron->transform = new EntityTransform(
		transform->x + transform->w * (sin(rot * TAU) * (nucleus + shell) / 2),
		transform->y + transform->h * (cos(rot * TAU) * (nucleus + shell) / 2),
		shell,
		shell);
	add(electron);
}

void loadEntities() {
	/*new EWood("wood-tiny", new EntityTransform(4.5, 3.5, 1, 1));
	new EWood("wood-big", new EntityTransform(3, 3, 2, 2));
	new EWood("wood-plank", new EntityTransform(3.5, 4.25, 3, 0.5));*/

	new EAtom("oxygen", new EntityTransform(5, 3, .5, .5), 8, 8);

	CText* counter = new CText("FPS: -", "ubuntu.ttf", 24);
	Entity* eCounter = new Entity("fps-counter", new EntityTransform(1, 1));
	eCounter->add(counter);
	eCounter->add(new CUpdate(std::function([counter = counter](){
		Threaded* thread = threads["render"];
		if (thread) {
			double fps = 1 / ((double) thread->tick.delta / toSeconds);
			counter->text->setString("FPS: " + std::to_string(fps) + " / " + std::to_string(frameLimit));

			// Set colour depending on how bad framerate is
			if (fps > 15) {
				if (fps > 45) {
					counter->text->setFillColor(Color::Green);
				} else {
					counter->text->setFillColor(Color(175, 175, 0));
				}
			} else {
				counter->text->setFillColor(Color::Red);
			}
		}
	})));

	if (debug) {
		new Entity("debug-label", new EntityTransform(5, 0, 1.25, 1, 1 / 16), {
			new CText("DEBUG", "ubuntu.ttf", 36, Color::Red)
		});
	}
}