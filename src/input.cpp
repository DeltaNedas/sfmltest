#include "input.h"

using namespace sf;

void callListeners(Event e) {
	switch (e.type) {
		case Event::KeyReleased:
		case Event::KeyPressed:
			for (KeyListener listener : keyListeners[e.key.code]) {
				listener(e.key, e.type == Event::KeyPressed);
			}
			// don't break in case a listener wants a KeyEvent's Event
		default:
			for (EventListener listener : listeners[e.type]) {
				listener(e);
			}
			break;
	}
}

template <class K, class V>
size_t add(K key, V listener, std::map<K, std::vector<V>>& listeners) {
	auto& vec = listeners[key];
	vec.push_back(listener);
	return vec.size() - 1;
}

template <class K, class V>
void remove(K key, size_t index, std::map<K, std::vector<V>>& listeners) {
	auto& vec = listeners[key];
	if (vec.size() > index) {
		vec.erase(vec.begin() + index);
	}
}

size_t addListener(Event::EventType type, EventListener listener) {
	return add(type, listener, listeners);
}

size_t addListener(Keyboard::Key key, KeyListener listener) {
	return add(key, listener, keyListeners);
}

void removeListener(Event::EventType type, size_t index) {
	remove(type, index, listeners);
}
void removeListener(Keyboard::Key key, size_t index) {
	remove(key, index, keyListeners);
}

std::map<Event::EventType, std::vector<EventListener>> listeners = {};
std::map<Keyboard::Key, std::vector<KeyListener>> keyListeners = {};