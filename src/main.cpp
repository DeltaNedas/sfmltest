#include "assets.h"
#include "entities.h"
#include "physics.h"
#include "threads.h"

#include <libgen.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <filesystem>
#include <functional>

namespace fs = std::filesystem;

typedef std::function<int(int, char**, int)> Call;

class Arg {
public:
	Arg(std::string name, Call call, std::string help = "", std::string params = "", size_t skip = 0) :
		Arg({
			std::string("-") + name[0],
			"--" + lower(name),
		},
		call,
		help,
		params,
		skip) {}

	Arg(std::vector<std::string> options, Call call, std::string help = "", std::string params = "", size_t skip = 0) {
		this->options = options;
		this->call = call;
		this->help = help;
		this->params = params;
		this->skip = skip;
	}

	std::vector<std::string> options = {};
	Call call;
	std::string help = "", params = "";
	size_t skip = 0;
};

std::vector<Arg> args = {
	Arg("help", std::function([](int argc, char** argv, int i) -> int {
		printf("Usage: %s [options]\n", argv[0]);
		printf("Available options:\n");
		for (Arg a : args) {
			printf("\t%s", concat(a.options, "/").c_str());
			if (a.params.size()) {
				printf(" %s", a.params.c_str());
			}
			if (a.help.size()) {
				printf(" - %s", a.help.c_str());
			}
			putc('\n', stdout);
		}
		exit(0);
	}), "Show this information and exit."),
	Arg("Version", std::function([](int argc, char** argv, int i) -> int {
		puts("SFMLTest v0.2.0");
		exit(0);
	}), "Show the program version and exit."),
	Arg("framelimit", std::function([](int argc, char** argv, int i) -> int {
		if (i == argc - 1) {
			fprintf(stderr, "[ERROR]\tExpected frame limit, in seconds, as argument #%d\n", i + 1);
			return EINVAL;
		}

		frameLimit = strtoul(argv[i + 1], NULL, 10);
		if (!frameLimit) {
			fprintf(stderr, "[ERROR]\tInvalid frame limit for argument #%d\n", i + 1);
			return EINVAL;
		}
		return 0;
	}), "Set target framerate. Default is 60", "<fps>", 1),
	Arg("root", std::function([](int argc, char** argv, int i) -> int {
		if (i == argc - 1) {
			fprintf(stderr, "[ERROR]\tExpected root directory as argument #%d\n", i + 1);
			return EINVAL;
		}

		root = std::string(argv[i + 1]);
		return 0;
	}), "Set root directory to use for reading content. Default is executable's parent", "<root>", 1),
	Arg("debug", std::function([](int argc, char** argv, int i) -> int {
		debug = true;
		return 0;
	}), "Disable signal handling for use in a debugger.")
};

void setRoot() {
	if (root.empty()) {
		char buffer[PATH_MAX] = ".";
		if (readlink("/proc/self/exe", buffer, sizeof(buffer) - 1) == -1) {
			fprintf(stderr, "[WARN]\tFalling back to current directory for content root.\n");
			root = "."; // Fallback to current directory, requires `cd build; ./sfmltest` to function properly
		} else {
			root = std::string(dirname(buffer));
		}
	}

	if (root[root.size() - 1] != '/') {
		root.push_back('/');
	}

	std::error_code code;
	if (!fs::is_directory(root), code) {
		fprintf(stderr, "[FATAL]\tContent root is not a directory!\n");
		throw ENOTDIR;
	}
}

void cleanUp() {
	if (window) {
		delete window;
	}
	if (world) {
		delete world;
	}
	for (Entity* e : entities) {
		delete e;
	}
	for (auto& it: threads) {
		delete it.second;
	}
}

char handled = 0;

void handleSignal(int signo) {
	if (handled++ < 2) {
		printf("Bye bye!\n");
		running = false;
	} else {
		printf("\n[FATAL]\tForce quitting!\n");
		exit(-1);
	}
}

int main(int argc, char** argv) {
	size_t skip = 0;
	for (int i = 1; i < argc; i++) {
		if (skip) {
			skip--;
			continue;
		}

		std::string arg(argv[i]);
		for (Arg a : args) {
			if (compare(arg, a.options)) {
				int code = a.call(argc, argv, i);
				if (code) {
					return code;
				}
				skip = a.skip;
				goto next;
			}

		}

		fprintf(stderr, "[ERROR]\tUnknown argument \"%s\"!\n", argv[i]);
		return EINVAL;

		next:
		continue;
	}

	if (!debug) {
		if (signal(SIGINT, handleSignal) == SIG_ERR
			|| signal(SIGABRT, handleSignal) == SIG_ERR
			|| signal(SIGSEGV, handleSignal) == SIG_ERR) {
			fprintf(stderr, "[FATAL]\tCan't handle signals!\n");
			return 1;
		}
	}

	try {
		setRoot();
		loadAssets();
		loadEntities(); // TODO: load it from assets/
	} catch (int e) {
		return e;
	}

	int code = start();
	cleanUp();
	return code;
}
