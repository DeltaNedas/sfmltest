#include "uuid.h"

#include "threads.h"

#include <string.h>

UUID::UUID() {
	uuid_generate_random(data);
	valid = true;
}

UUID::UUID(uuid_t data) {
	memcpy(this->data, data, 16 * sizeof (unsigned char));
	valid = true;
}

UUID::UUID(std::string parse) {
	valid = !uuid_parse(parse.c_str(), data);
}

UUID::operator std::string() const {
	if (!valid) {
		return "";
	}

	char* buffer = (char*) malloc(sizeof(char) * 37); // 32 for data, 4 for -'s and 1 for \0
	if (!buffer) {
		quit(ENOMEM);
		return "";
	}

	uuid_unparse_lower(data, buffer);
	std::string ret(buffer);
	free(buffer);
	return ret;
}