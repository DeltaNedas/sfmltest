#include "vars.h"

using namespace sf;

// Default values for variables.

RenderWindow* window = nullptr;

bool debug = false;
nanos_t frameLimit = 60; // Default, set with `-f` option.
nanos_t frameRate = -1;
const nanos_t tickRate = toSeconds / 60; // For physics and update threads
const nanos_t inputRate = toSeconds / 60; // 240Hz for input thread.
const float pixelScale = 32;