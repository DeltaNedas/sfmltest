#include "assets.h"

using namespace sf;

std::string root = "";

template <typename T>
void loadAsset(std::map<std::string, T*>& values, std::string file, std::string name) {
	T* val = new T();
	if (!val->loadFromFile(file)) {
		throw errno;
	}
	values[name] = val;
}

void loadAssets() {
	fonts = Assets<Font>("assets/fonts", "default.ttf", std::function([](std::string file, std::string name) {
		loadAsset(fonts.values, file, name);
	}));
	textures = Assets<Texture>("assets/textures", "default.png", std::function([](std::string file, std::string name) {
		loadAsset(textures.values, file, name);
	}));

	fonts.load();
	textures.load();
}

Assets<Font> fonts;
Assets<Texture> textures;