#include "components.h"

#include "assets.h"
#include "entities.h"
#include "vars.h"

using namespace sf;

Component::Component() {
	this->uuid = UUID();
	components.insert(this);
}
Component::~Component() {
	components.erase(this);
}
UUID Component::getUUID() {
	return uuid;
}
void Component::added() {}

CTransform::CTransform(EntityTransform* transform) : Component() {
	this->transform = transform;
	transformComponents.insert(this);
}
CTransform::~CTransform() {
	transformComponents.erase(this);
}
void CTransform::added() {
	if (!transform) {
		transform = entity->transform;
	}
}

CHealth::CHealth(float maxHealth) : Component() {
	this->maxHealth = maxHealth;
	this->health = health;
	healthComponents.insert(this);
}
CHealth::~CHealth() {
	healthComponents.erase(this);
}


CDrawable::CDrawable(Drawable* drawable) : CTransform() {
	this->drawable = drawable;
	drawableComponents.insert(this);
}
CDrawable::~CDrawable() {
	if (drawable) {
		delete drawable;
	}
	drawableComponents.erase(this);
}

CSprite::CSprite(std::string file) : CDrawable() {
	Texture* texture = textures.get(file);
	sprite = new Sprite(*texture);
	Vector2 size = texture->getSize();
	sprite->setScale(pixelScale / size.x, pixelScale / size.y);

	drawable = sprite;
	spriteComponents.insert(this);
}
CSprite::CSprite(Sprite* sprite) : CDrawable() {
	this->sprite = sprite;

	drawable = this->sprite;
	spriteComponents.insert(this);
}
CSprite::~CSprite() {
	spriteComponents.erase(this);
}


CShape::CShape(size_t sides, float length) : CDrawable() {
	if (sides == 4) {
		shape = new RectangleShape(Vector2f(length, length));
	} else {
		shape = new CircleShape(length, sides);
	}
	drawable = shape;
	shapeComponents.insert(this);
}
CShape::CShape(Shape* shape) : CDrawable() {
	this->shape = shape;
	drawable = this->shape;
	shapeComponents.insert(this);
}
CShape::~CShape() {
	shapeComponents.erase(this);
}


CText::CText(std::string text, std::string font, size_t size, Color color) : CDrawable() {
	this->font = fonts.get(font);
	this->text = new Text(text, *this->font, size);
	this->text->setFillColor(color);
	this->text->setScale(size / pixelScale / 4, size / pixelScale / 4);

	drawable = this->text;
	textComponents.insert(this);
}
CText::~CText() {
	textComponents.erase(this);
}


CUpdate::CUpdate(std::function<void(void)> func) : Component() {
	this->func = func;
	updateComponents.insert(this);
}
CUpdate::~CUpdate() {
	updateComponents.erase(this);
}

CPhysics::CPhysics(float mass, float friction) : CTransform() {
	this->mass = mass;
	this->friction = friction;

	physicsComponents.insert(this);
}
CPhysics::~CPhysics() {
	physicsComponents.erase(this);
}


std::set<Component*> components = {};
std::set<CTransform*> transformComponents = {};
std::set<CHealth*> healthComponents = {};

std::set<CDrawable*> drawableComponents = {};
std::set<CSprite*> spriteComponents = {};
std::set<CShape*> shapeComponents = {};
std::set<CText*> textComponents = {};

std::set<CUpdate*> updateComponents = {};
std::set<CPhysics*> physicsComponents = {};