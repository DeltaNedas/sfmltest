BUILDDIR ?= build
OBJECTDIR ?= objects
SOURCEDIR ?= src
INCLUDEDIR ?= include

UNAME := $(shell uname)
CXX ?= g++
CXXFLAGS ?= -O3 -Wall -ansi -pedantic -std=c++2a -I$(INCLUDEDIR) -c -g
LDFLAGS ?= '-Wl,-rpath,$$ORIGIN'
LDFLAGS += $(shell pkg-config --libs-only-l sfml-all)
LDFLAGS += -luuid
LDFLAGS += -lBox2D
ifeq ($(UNAME), Linux)
	LDFLAGS += -lpthread -lX11
endif

BINARY ?= sfmltest
SOURCES := $(shell find $(SOURCEDIR)/ -type f -name "*.cpp")
OBJECTS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.o, $(SOURCES))
DEPENDS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.d, $(SOURCES))

all: $(BUILDDIR)/$(BINARY)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%
	@printf "CC\t$@\n"
	@mkdir -p `dirname $@`
	@$(CXX) $(CXXFLAGS) -MMD -MP $< -o $@

-include $(DEPENDS)

$(BUILDDIR)/$(BINARY): $(OBJECTS)
	@printf "CCLD\t$@\n"
	@mkdir -p $(BUILDDIR)
	@$(CXX) $^ -o $@ $(LDFLAGS)

clean:
	rm -rf $(OBJECTDIR)
